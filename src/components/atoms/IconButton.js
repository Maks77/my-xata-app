import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import {Ionicons} from "@expo/vector-icons";

const IconButton = ({text, onPress, style}) => {

  return (
    <TouchableOpacity style={[styles.container, style]} activeOpacity={0.7} onPress={onPress}>
      <Ionicons style={{marginRight: 8}} name="ios-add-circle" size={24} color="#34C672" />
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#000000'
  }
})
export default IconButton
