import React, {useState} from 'react'
import {View, Text, Image, StyleSheet, TextInput, SafeAreaView, StatusBar} from 'react-native'
import SmoothPinCodeInput from "react-native-smooth-pincode-input";
import Color from '../../styles/colors'
import {LinkButton} from "../../components/atoms";
import general from "../../styles/general";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import LocalStorage from "../../utils/LocalStorage";
import { CommonActions } from '@react-navigation/native';


const PasswordScreen = ({navigation}) => {
  const [password, setPasswordCode] = useState('')

  const _checkCode = () => {}
  const _cleanPassword = () => setPasswordCode('')
  const _skipPressed = () => {
    // navigation.replace('HomeNavigator')
    // navigation.dispatch(resetAction)
    resetStack()
  }

  /**
   * Use to reset navigation stack
   */
  const resetStack = () => {
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'HomeNavigator' }
        ],
      })
    );
  }

  return (
    <KeyboardAwareScrollView
        style={{backgroundColor: Color.white}}
        contentContainerStyle={{ flexGrow: 1 }}
        behavior={'position'}
        scrollEnabled={true}
        enableOnAndroid={true}
        enableAutomaticScroll={true}
        extraScrollHeight={40}
    >
      <StatusBar backgroundColor={Color.transparent}/>
      <View style={styles.halfContainer}>
        <Image style={styles.bgImage} source={require('../../../assets/images/password_bg.png')} />
      </View>
      <View style={[styles.halfContainer, {justifyContent: 'flex-start'}]}>
        <Text style={styles.confirmText}>Придумайте 4-х значный пароль для входа</Text>

        <View style={styles.confirmHolder}>
          <SmoothPinCodeInput
            password={true}
            value={password}
            onTextChange={code => setPasswordCode(code)}
            onFulfill={_checkCode}
            textStyle={{
              fontFamily: 'comfortaa',
              fontSize: 24,
              color: '#333333',
            }}
            cellStyle={{
              borderBottomWidth: 2,
              borderColor: 'gray',
              flex: 1,
              width: 30
            }}
            cellSpacing={10}
            cellStyleFocused={{
              borderColor: Color.accent
            }}
            codeLength={4}
          />

          {
            password !== '' ?
            <LinkButton onPress={_cleanPassword} style={{marginTop: 20}} text={'Очистить'} textColor={Color.accent}/>
            :
            <LinkButton onPress={_skipPressed} style={{marginTop: 20}} text={'Пропустить'} textColor={Color.accent}/>
          }

        </View>

      </View>
    </KeyboardAwareScrollView>
  )
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  halfContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bgImage: {},
  confirmText: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    textAlign: 'center',
    color: '#333',
    fontWeight: 'normal',
    paddingHorizontal: 40
  },


  confirmHolder: {
    alignItems: 'center',
  }
})



export default PasswordScreen
