import React from "react";
import {StyleSheet, Text, View, Image} from "react-native";
import EditRoundButton from "../atoms/EditRoundBtn";
import Icon from "../../utils/icon";
import Color from "../../styles/colors";

const PropertySlideCard = ({property, onEditPress}) => {
  return (
    <View style={styles.container}>
      <EditRoundButton
        onPress={onEditPress}
        style={{position: 'absolute', right: 20, top: 20, elevation: 4, zIndex: 1}}
      />
      <View style={styles.imageContainer}>
        <Image source={{uri: property.snapshot}} style={{flex: 1, width: '100%'}} />
      </View>
      <View style={styles.bottomContainer}>
        <Icon style={styles.aimIcon} name={'aim'} size={22} color={'#191919'} />
        <Text style={styles.address}>
          {property.city}, {property.street}, {property.house_number}, кв. {property.apartment_number}
        </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    borderRadius: 10,
    overflow: 'hidden',
    flex: 1
  },

  imageContainer: {
    flex: 1,
    backgroundColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center'
  },

  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Color.white
  },

  aimIcon: {
    padding: 17
  },

  address: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#191919'
  }
});

export default PropertySlideCard
