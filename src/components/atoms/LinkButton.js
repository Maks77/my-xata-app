import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native'
import styled from 'styled-components'

const LinkButton = ({text, textColor, onPress, style}) => {

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.buttonWrapper, style]} onPress={onPress} activeOpacity={0.7}>
        <Text style={[styles.text, textColor && { color: textColor }]}>{text}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  buttonWrapper: {
    paddingHorizontal: 3,
    paddingVertical: 8
  },
  text: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    color: '#828282',
    fontWeight: 'normal'
  }

})

export default LinkButton
