import {Pagination} from "react-native-snap-carousel";
import React from "react";
import {View, StyleSheet} from 'react-native'

const renderPagination = ({slidesLength, activeSlide, padding}) => {


  if (slidesLength > 1) {
    return (
      <Pagination
        dotsLength={slidesLength}
        activeDotIndex={activeSlide}
        containerStyle={{
          paddingVertical: padding ? padding : 10
        }}
        dotContainerStyle={{
          padding: 0
        }}
        dotStyle={{
          width: 6,
          height: 6,
          borderRadius: 3,
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
        }}

        inactiveDotStyle={{
          // Define styles for inactive dots here
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        <View style={styles.singleDot} />
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    alignItems: 'center'
  },
  singleDot: {
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  }
})

export default renderPagination
