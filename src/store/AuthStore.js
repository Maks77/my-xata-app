import {action, observable, computed} from "mobx";
import api from "../api/api";
import {apiConfig} from "../config/config";

export class AuthStore {

  constructor() {}

  @observable phoneNumber = ''
  @observable confirmCode = ''
  @observable access_token = ''
  @observable refresh_token = ''

  @action setAuthData = (data) => {
    const {refresh_token, access_token} = data
    this.access_token = access_token
    this.refresh_token = refresh_token
  }

  @action auth = (phoneNumber) => {
    const body = {
      user_phone: phoneNumber,
    }
    this.phoneNumber = phoneNumber
    return api.post('/oauth2/get-otp/', body).then(res => {
      console.log('otp_code', res)
      if (res.status === 200 || res.status === 201) {
        this.confirmCode = res.data.otp_code ? res.data.otp_code : ''
      }
      return res
    })
  }

  @action token = () => {
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    const formData = new FormData();
    formData.append('username', this.phoneNumber);
    formData.append('password', this.confirmCode);
    formData.append('grant_type', 'password');
    formData.append('client_id', apiConfig.client_id);
    formData.append('client_secret', apiConfig.client_secret);

    return api.post('/oauth2/token/', formData, {headers: headers})
      .then((res) => {
        if (res.status === 200) {
          const {access_token, refresh_token} = res.data
          this.access_token = access_token
          this.refresh_token = refresh_token
        }
        return res
      })
  }

  @action getUser = () => {
    return api.get('/api/user')
  }
}
