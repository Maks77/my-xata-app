import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View
} from 'react-native'
import UnderlinedInput from "../../components/UnderlinedInput";


const  AddRealtyForm = ({realty}) => {
  const [editable, setEditable] = useState(true)
  const [formData, setFormData] = useState({
    id: null,
    realty_type: '',
    city: '',
    street: '',
    house_number: '',
    building_number: '',
    apartment_number: ''
  })

  useEffect(() => {
    if (realty && realty.id !== 'custom_empty') {
      setEditable(false)
      setFormData(realty)
    } else {
      setFormData({})
      setEditable(true)
    }
  }, [realty])

  const _handleInput = (key, value) => {
    setFormData(prevState => ({
      ...prevState,
      [key]: value
    }))
  }

  return (
    <View>
      <UnderlinedInput
        editable={editable}
        returnKeyType={'next'}
        style={formStyle.lineInput}
        placeholder={'Город*'}
        value={formData.city}
        onChangeText={(t) => _handleInput('city', t)}
      />
      <UnderlinedInput
        editable={editable}
        returnKeyType={'next'}
        style={formStyle.lineInput}
        placeholder={'Улица*'}
        value={formData.street}
        onChangeText={(t) => _handleInput('street', t)}
      />
      <View style={formStyle.row}>
        <UnderlinedInput
          editable={editable}
          returnKeyType={'next'}
          style={formStyle.rowInput}
          placeholder={'Дом*'}
          value={formData.house_number}
          onChangeText={(t) => _handleInput('house_number', t)}
        />
        <UnderlinedInput
          editable={editable}
          returnKeyType={'next'}
          style={formStyle.rowInput}
          placeholder={'Корпус'}
          value={formData.building_number}
          onChangeText={(t) => _handleInput('building_number', t)}
        />
        <UnderlinedInput
          editable={editable}
          returnKeyType={'done'}
          style={formStyle.rowInput}
          placeholder={'Кв'}
          value={formData.apartment_number}
          onChangeText={(t) => _handleInput('apartment_number', t)}
        />
      </View>
    </View>
  )
}

const formStyle = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: -16
  },
  lineInput: {
    marginBottom: 10
  },
  rowInput: {
    width: 'auto',
    flex: 1,
    marginHorizontal: 16
  }
})

export default AddRealtyForm;
