import React from 'react'
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native'
import styled from 'styled-components'

const YellowButton = ({text, disabled, onPress, style}) => {

  return (
    <TouchableOpacity
      style={[styles.container, disabled && styles.disabled, style]}
      disabled={disabled}
      onPress={!disabled ? onPress : () => {}}
    >
        <View style={styles.innerContainer}>
          <Text style={styles.buttonText}>{text}</Text>
        </View>

    </TouchableOpacity>
  )
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFAD08',
    borderRadius: 5,
    paddingVertical: 15,
    opacity: 1
  },

  disabled: {
    opacity: 0.5
  },

  innerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonText: {
    fontFamily: 'proxima-nova-regular',
    color: '#333',
    fontSize: 17
  }
})

export default YellowButton
