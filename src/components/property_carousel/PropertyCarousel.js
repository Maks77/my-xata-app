import React, {useState, useRef, useEffect} from "react";
import {Dimensions, StyleSheet, Text, View} from "react-native";
import Color from "../../styles/colors";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import PropertySlideCard from "./RealtyCard";
import EmptyCard from "./EmptyCard";

const screen = Dimensions.get('screen')

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH - 20);

// const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);

const PropertyCarousel = ({realtyArr, onChangeActiveSlide, onEditPress}) => {
  const [activeSlide, setSlide] = useState(0)
  const [emptyArr, setEmptyArr] = useState(true)

  const carouselRef = useRef(null)

  useEffect(() => {
    onChangeActiveSlide(activeSlide)
  }, [activeSlide, realtyArr])

  const setActiveSlide = (slideInd) => {
    setSlide(slideInd)
  }

  const _renderSlides = ({item, index}) => {
    if (!item || item.id === 'custom_empty') {
      return <EmptyCard property={item} />
    } else {
      return <PropertySlideCard onEditPress={onEditPress} property={item} />
    }
  }

  return (
    <Carousel
      ref={carouselRef}
      data={realtyArr}
      renderItem={_renderSlides}
      sliderWidth={SLIDER_WIDTH}
      inactiveSlideShift={0}
      itemWidth={ITEM_WIDTH}
      onSnapToItem={(index) => setSlide(index)}
    />
  )
}


export default PropertyCarousel
