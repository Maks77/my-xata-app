import React, {useState, useEffect, useRef} from 'react'
import {StyleSheet, View, Text, Image, SafeAreaView, Dimensions, StatusBar} from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {LinkButton} from "../../components/atoms";
import Color from "../../styles/colors";
import renderPagination from "../../components/carouselPagination";


const screen = Dimensions.get('screen')
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);

const slides = [
  {
    title: 'Вся ваша недвижимость в одном приложении',
    subTitle: 'Просто введите данные вашей недвижимости и оплачивайте коммунальные через приложение',
    img: require('../../../assets/images/intro_bg_1.png')
  },
  {
    title: 'Экономия времени',
    subTitle: 'Нет нужды стоять в длинных очередях - оплачивайте с любой точки мира с помощью вашего смартфона',
    img: require('../../../assets/images/intro_bg_2.png')
  },
  {
    title: 'Статистика',
    subTitle: 'Вся информация о коммунальных услугах, включая квитанции',
    img: require('../../../assets/images/intro_bg_3.png')
  },
]



const IntroScreen = ({navigation}) => {
  const [activeSlide, setSlide] = useState(0)
  const carouselRef = useRef(null)

  const renderSlide = ({item, index}) => {
    return (
      <View style={styles.sliderContainer}>
        <Image style={styles.sliderImage} source={item.img} />

        <View style={styles.sliderTextContainer}>
          <Text style={styles.sliderTitle}>{item.title}</Text>
          <Text style={styles.sliderSubTitle}>{item.subTitle}</Text>
        </View>

      </View>
    )
  }

  const pagination = () => {
    return (
      <Pagination
        dotsLength={slides.length}
        activeDotIndex={activeSlide}
        containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}
        dotStyle={{
          width: 6,
          height: 6,
          borderRadius: 3,
          marginHorizontal: -5,
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
        }}
        inactiveDotStyle={{
          // Define styles for inactive dots here
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }


  const onSkipPressed = () => {
    navigation.navigate('LoginNavigator')
  }

  const onButtonPress = () => {
    console.log('activeSlide', activeSlide)
    if (activeSlide === slides.length - 1) {
      carouselRef.current.snapToItem(0)
    } else {
      carouselRef.current.snapToNext()
    }
  }

  useEffect(() => {
    // console.log('activeSlide', activeSlide)
  })

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Color.transparent}/>
      <Carousel
        ref={carouselRef}
        data={slides}
        renderItem={renderSlide}
        sliderWidth={SLIDER_WIDTH}
        inactiveSlideShift={0}
        itemWidth={screen.width}
        onSnapToItem={(index) => setSlide(index)}
      />
      <View style={styles.bottomContainer}>
        <LinkButton onPress={onSkipPressed} text={'Пропустить'} />
        {/*{pagination()}*/}
        {renderPagination({slidesLength: slides.length, activeSlide: activeSlide})}
        <LinkButton onPress={onButtonPress} text={'Следующий'} textColor={'#333333'} />
      </View>

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sliderContainer: {
    alignItems: 'center',
    flex: 1,
    backgroundColor: Color.white
  },
  sliderTextContainer: {
    flex: 1,
    paddingHorizontal: 30
  },
  sliderTitle: {
    fontFamily: 'proxima-nova-bold',
    fontWeight: 'bold',
    fontSize: 22,
    color: '#333333',
    textAlign: 'center',
    marginBottom: 20
  },
  sliderSubTitle: {
    fontFamily: 'proxima-nova-regular',
    fontWeight: 'normal',
    color: '#333333',
    fontSize: 15,
    textAlign: 'center',
  },
  sliderImage: {
    flex: 1,
    resizeMode: 'contain'
  },



  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#fff',
    paddingHorizontal: 10,
    backgroundColor: Color.white,
  },
  bottomContainer: {
    backgroundColor: Color.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingBottom: 20
  }
})


export default IntroScreen
