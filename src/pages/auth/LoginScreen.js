import React, {useRef, useEffect, useState} from 'react'
import { StyleSheet, Dimensions, View, Text, TextInput, AsyncStorage } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';
import general from "../../styles/general";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {LinkButton, Logo} from "../../components/atoms";
import YellowButton from "../../components/atoms/YellowButton";
import Color from "../../styles/colors";
import {StatusBar} from "react-native-web";
import UnderlinedInput from "../../components/UnderlinedInput";
import loginService from "../../api/loginService";
import LocalStorage from '../../utils/LocalStorage'
import withStore from "../../utils/helpers/withStore";


const screen = Dimensions.get('screen');

const LoginScreen = (props) => {
  const {store, navigation} = props
  const [phoneNumber, setPhoneNumber] = useState('+380998888888')

  const onEnterPressed = () => {
    LocalStorage.setItem('phoneNumber', phoneNumber)
      .then(() => {
        store.authStore.phoneNumber = phoneNumber
        navigation.navigate('ConfirmScreen')
      })
      .catch(e => {
        console.error('[LoginScreen] phoneNumber_LocalStorage_error', e)
      })
  }


  useEffect(() => {
  }, [])


  return (
    <KeyboardAwareScrollView
      style={{backgroundColor: Color.white}}
      contentContainerStyle={{ flexGrow: 1 }}
      behavior={'position'}
      scrollEnabled={true}
      enableOnAndroid={true}
      enableAutomaticScroll={true}
      extraScrollHeight={40}
    >
      <View style={styles.container}>
        <StatusBar backgroundColor={Color.white}/>
        <View style={[styles.halfContainer, {flex: 0, height: screen.height / 2}]}>
          <Logo />
        </View>

        <View style={[styles.halfContainer, {justifyContent: 'flex-start'}]}>

          <UnderlinedInput
            keyboardType={'number-pad'}
            style={{marginBottom: 16}}
            placeholder={'Номер'}
            value={phoneNumber}
            leftIcon={ () => <FontAwesome name="mobile-phone" size={22} color="#828282" />}
            onChangeText={text => setPhoneNumber(text)}
           />

          <YellowButton onPress={onEnterPressed} text={"Войти"} disabled={false} />

          <View style={styles.textContainer}>
            <Text style={{...general.smallTextGray, marginBottom: 8}}>Нажимая, я соглашаюсь с условиями</Text>
            <Text style={general.smallTextYellow}>«Пользовательского соглашения»</Text>
          </View>
        </View>
      </View>
    </KeyboardAwareScrollView>

  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.white,
    flex: 1,
    alignItems: 'center',
    marginVertical: 0,
    marginHorizontal: 15
  },
  halfContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  textContainer: {
    marginTop: 20,
    paddingVertical: 0,
    paddingHorizontal: 25
  }
})



// export default LoginScreen
export default withStore(LoginScreen)
