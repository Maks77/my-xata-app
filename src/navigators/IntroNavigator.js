import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';

// Screens
import IntroScreen from "../pages/auth/IntroScreen";


const Stack = createStackNavigator();


const IntroNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{
      headerShown: false
    }}>
      {/*<Stack.Screen name="SplashScreen" component={SplashScreen} />*/}
      <Stack.Screen name="IntroScreen" component={IntroScreen}/>
    </Stack.Navigator>
  )
}

export default IntroNavigator
