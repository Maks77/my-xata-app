import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';


// Screens
import AccountMenuScreen from "../pages/account/AccountMenuScreen";

const Stack = createStackNavigator();

const AccountNavigator = () => {
  const screenOptions = {
    headerHideShadow: true,
    headerStyle: {
      elevation: 0
    },
    headerTitleStyle: {
      fontSize: 17,
      fontFamily: 'proxima-nova-regular',
      fontWeight: 'normal',
      color: '#000000'
    }
  }

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen name="AccountMenuScreen" component={AccountMenuScreen}/>
    </Stack.Navigator>
  )
}

export default AccountNavigator
