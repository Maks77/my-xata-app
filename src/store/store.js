import {AuthStore} from './AuthStore'
import {RealtyStore} from "./RealtyStore";

export class Store {

  constructor() {
    this.authStore = new AuthStore()
    this.realtyStore = new RealtyStore()
  }
}


