import React, {useState, useEffect} from 'react';
import {StyleSheet, View, TextInput} from 'react-native'
import Color from "../styles/colors";

const UnderlinedInput = ({
                           placeholder,
                           leftIcon,
                           rightIcon,
                           style,
                           onSubmitEditing,
                           onFocus,
                           onBlur,
                           keyboardType,
                           onChangeText,
                           value,
                           returnKeyType,
                           editable,
                           invalid
                         }) => {
  const [focused, setFocused] = useState(false)


  const _onFocus = () => {
    setFocused(true)
    onFocus && onFocus()
  }

  const _onBlur = () => {
    setFocused(false)
    onBlur && onBlur()
  }

  return (
    <View style={[
      styles.inputContainer,
      invalid && styles.invalidInputContainer,
      focused && {borderBottomColor: Color.accent},
      style
    ]}>
      {leftIcon && leftIcon()}
      <TextInput
        returnKeyType={returnKeyType || 'done'}
        style={styles.input}
        keyboardType={keyboardType}
        placeholder={placeholder}
        onSubmitEditing={onSubmitEditing}
        onFocus={_onFocus}
        onBlur={_onBlur}
        value={value}
        onChangeText={onChangeText}
        editable={editable}
      />
      {rightIcon && rightIcon()}
    </View>
  )
}


const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  invalidInputContainer: {
    borderBottomColor: 'red',
  },
  textContainer: {
    marginTop: 20,
    paddingVertical: 0,
    paddingHorizontal: 25
  },
  input: {
    flex: 1,
    fontFamily: 'proxima-nova-regular',
    color: '#191919',
    fontSize: 17,
    width: '100%',
    paddingVertical: 7,
    paddingHorizontal: 10
  }
})


export default UnderlinedInput
