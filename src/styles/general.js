import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  smallTextGray: {
    fontFamily: 'proxima-nova-regular',
    color: '#828282',
    fontSize: 15,
    textAlign: 'center'
  },
  smallTextYellow: {
    fontFamily: 'proxima-nova-regular',
    color: '#FFAD08',
    fontSize: 15,
    textAlign: 'center'
  },
  roundSectionTitle: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#828282',
    textAlign: 'center',
  }
});
