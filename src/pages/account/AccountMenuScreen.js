import React, {useState, useLayoutEffect} from 'react'
import {View, Text, Image, TouchableHighlight, TouchableOpacity, StyleSheet, TextInput, ScrollView, StatusBar, SafeAreaView} from 'react-native'
import SmoothPinCodeInput from "react-native-smooth-pincode-input";
import Color from '../../styles/colors'
import general from "../../styles/general";
import Icon from '../../utils/icon'
import { Feather } from "@expo/vector-icons";

const account_menu = [
  {id: 0, icon: 'card', title: 'Способы оплаты', titleColor: '#000000', iconColor: Color.accent},
  {id: 1, icon: 'user-invite', title: 'Пригласить', titleColor: '#000000', iconColor: Color.accent},
  {id: 2, icon: 'earth', title: 'Язык', titleColor: '#000000', iconColor: Color.accent},
  {id: 3, icon: 'headphones', title: 'Служба поддержки', titleColor: '#000000', iconColor: Color.accent},
  {id: 4, icon: 'lock', title: 'Код пароль и Face ID', titleColor: '#000000', iconColor: Color.accent},
  {id: 5, icon: 'info', title: 'О приложении', titleColor: '#000000', iconColor: Color.accent},
  {id: 6, icon: 'exit', title: 'Выйти', titleColor: '#EB5757', iconColor: '#EB5757'}
]

const HeaderCloseIcon = () => (
  <TouchableOpacity>
    <Icon name={'close'} size={24} />
  </TouchableOpacity>
)



const AccountMenuScreen = ({navigation}) => {

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Аккаунт',
      headerRight: () => (<HeaderCloseIcon />)
    })
  })

  const _renderMenuItem = (menuItem) => {
    const {id, icon, title, titleColor, iconColor} = menuItem
    return (
      <TouchableOpacity key={id} activeOpacity={0.2} style={styles.menuItemContainer}>
        <View style={{width: 32, alignItems: 'center'}}>
          <Icon name={icon} size={20} color={iconColor}/>
        </View>
        <Text style={[styles.menuItemTitle, {color: titleColor}]}>{title}</Text>
      </TouchableOpacity>
    )
  }

  return (
    <ScrollView style={styles.container}>
      <StatusBar barStyle={'dark-content'} backgroundColor={Color.white}/>
      <ProfileTab />
      {account_menu.map(el => _renderMenuItem(el))}
    </ScrollView>
  )
}

const ProfileTab = () => {
  return (
    <View style={s.container}>
      <View style={s.imgHolder}>
        <Text>img</Text>
      </View>
      <View style={s.contentHolder}>
        <Text style={s.userName}>Артем Белоконь</Text>
        <Text style={s.userPhone}>+38 (073) 101 10 01</Text>
      </View>
      <TouchableOpacity>
        <Feather name="edit-2" size={20} color="#191919" />
      </TouchableOpacity>
    </View>
  )
}

const s = StyleSheet.create({
  container: {
    paddingHorizontal: 17,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 30
  },
  imgHolder: {
    width: 60,
    height: 60,
    borderRadius: 30,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ccc'
  },
  contentHolder: {
    marginLeft: 14,
    justifyContent: 'space-around',
    flex: 1
  },
  userName: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    color: '#000'
  },
  userPhone: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#8A8A8F'
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.white
  },
  menuItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 12
  },
  menuItemTitle: {
    marginLeft: 17,
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    color: '#000000'
  }
})

export default AccountMenuScreen
