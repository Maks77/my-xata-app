import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import Color from "../../styles/colors";
import Icon from "../../utils/icon";
import { Feather } from '@expo/vector-icons';


const EditRoundButton = ({style, onPress}) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress} style={[styles.container, style]}>
      <Feather name="edit-2" size={15} color="#191919" />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 30,
    height: 30,
    backgroundColor: Color.white,
    borderRadius: 15
  }
})

export default EditRoundButton
