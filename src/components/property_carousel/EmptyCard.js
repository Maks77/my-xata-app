import React from "react";
import {StyleSheet, Text, View} from "react-native";
import Icon from "../../utils/icon";
import Color from "../../styles/colors";

const EmptyCard = ({property}) => {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Text style={{color: '#fff'}}>No image</Text>
      </View>
      <View style={styles.bottomContainer}>
        <Icon style={styles.aimIcon} name={'aim'} size={22} color={'#191919'} />
        <Text style={styles.address}>
          Не добавлено
        </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    borderRadius: 10,
    overflow: 'hidden',
    flex: 1
  },

  imageContainer: {
    flex: 1,
    backgroundColor: '#191919',
    alignItems: 'center',
    justifyContent: 'center'
  },

  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Color.white
  },

  aimIcon: {
    padding: 17
  },

  address: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#191919'
  }
});

export default EmptyCard
