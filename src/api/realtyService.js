import api from "./api";

const addRealty = (data) => {
    const body = {
        realty_type: "Living",
        city: data.city,
        street: data.street,
        house_number: data.house_number,
        building_number: data.building_number || '',
        apartment_number: data.apartment_number || '',
    }
    return api.post('/api/realty/', body)
}

const updateRealty = (realtyData) => {
    const body = {
        realty_type: "Living",
        city: realtyData.city,
        street: realtyData.street,
        house_number: realtyData.house_number,
        building_number: realtyData.building_number || '',
        apartment_number: realtyData.apartment_number || '',
    }
    return api.put(`/api/realty/${realtyData.id}`, body)
}

const deleteRealty = (id) => {
    return api.delete(`/api/realty/${id}`)
}

const getRealty = () => {
    return api.get('/api/realty/')
}


export default {
    getRealty,
    addRealty,
    deleteRealty,
    updateRealty
}
