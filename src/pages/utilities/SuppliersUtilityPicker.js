import React, {useState, useEffect, useLayoutEffect} from 'react'
import {View, Text, StyleSheet, Picker} from 'react-native'
import Color from "../../styles/colors";
import utilityService from "../../api/utilityService";

const SuppliersUtilityPicker = (props) => {
  const {route} = props
  const {supplier} = route.params

  // const [mSupplier, setMSupplier] = useState(supplier)
  const [pickedUtility, setPickedUtility] = useState(null)

  console.log('SuppliersUtilityPicker', supplier)

  useEffect(() => {
    console.log('onPick', pickedUtility)
  }, [pickedUtility])

  const pickUtilityHandler = (utilityId, index) => {
    console.log('pickUtilityHandler', utilityId)
    setPickedUtility(utilityId)
    if (utilityId) {
      _getSupplierAuthFields(utilityId)
    }
  }

  // Render Picker items
  const _renderUtilitiesItem = (utilities) => {
    return (
        utilities.map(el => <Picker.Item key={el.id} label={el.name} value={el.id} />)
    )
  }

  const _getSupplierAuthFields = (utilityId) => {
    utilityService.getAuthFields(utilityId).then(res => {
      console.log('_getSupplierAuthFields', res)
    })
  }

  return (
    <View style={styles.container}>
      <Text style={{textAlign: 'center'}}>{supplier.name}</Text>

      <View style={styles.pickerContainer}>
        <Picker
          style={styles.picker}
          selectedValue={pickedUtility}
          onValueChange={pickUtilityHandler}
          itemStyle={{textAlign: 'center', color: 'red'}}
        >
          <Picker.Item label='Услуга' value='' />
          {_renderUtilitiesItem(supplier.utilities)}
        </Picker>
      </View>

      {/* Render incoming fields */}

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.white
  },
  pickerContainer: {
    flexDirection: 'row',
    height: 45,
    marginHorizontal: 8,
    paddingBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)'
  },
  picker: {
    flex: 1,
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    color: '#828282'
  }
})

export default SuppliersUtilityPicker
