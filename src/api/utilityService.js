import api from "./api";

const getUtilities = () => {
    return api.get('/api/utility/')
}


const addUtility = (body) => {
    const testBody = {
        "name": "string",
        "supplier": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "category": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    }
    return api.post(`/api/utility/`, body)
}

const getRealtyUtilityCategories = (realtyId) => {
    return api.get(`/api/realty/${realtyId}/categories`)
}

const getRealtySuppliersByCategory = (realtyId, categoryId) => {
    return api.get(`/api/realty/${realtyId}/category/${categoryId}/suppliers`)
}

// Получение полей услуги для авторизации
const getAuthFields = (utilityId) => {
    return api.get(`/api/utility/${utilityId}/auth_fields`)
}

export default {
    getUtilities,
    addUtility,
    getRealtyUtilityCategories,
    getRealtySuppliersByCategory,
    getAuthFields
}
