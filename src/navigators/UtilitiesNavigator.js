import React from 'react'
import {StyleSheet} from 'react-native'
import {createStackNavigator} from '@react-navigation/stack';
import Colors from '../styles/colors'
// Screens
import UtilitiesList from "../pages/utilities/UtilitiesList";
import SuppliersList from "../pages/utilities/SuppliersList";
import SuppliersUtilityPicker from "../pages/utilities/SuppliersUtilityPicker";

const Stack = createStackNavigator();

const UtilitiesNavigator = () => {

  const screenOptions = {
    headerMode: 'screen',
    headerHideShadow: true,
    headerStyle: {
      elevation: 0,
      backgroundColor: Colors.accent,
    },
    headerTitleStyle: {
      fontSize: 17,
      fontFamily: 'proxima-nova-regular',
      fontWeight: 'normal',
      color: '#000000'
    },
  }

  return (
    <Stack.Navigator
      screenOptions={screenOptions}
    >
      <Stack.Screen
        title={'Выбор услуг'}
        name={'UtilitiesList'}
        component={UtilitiesList}
      />
      <Stack.Screen
        title={'Поиск по компании'}
        name={'SuppliersList'}
        component={SuppliersList}
        options={{
          headerStyle: {
            elevation: 0,
            backgroundColor: '#fff'
          }
        }}
      />
      <Stack.Screen
        title={'company name (set)'}
        name={'SuppliersUtilityPicker'}
        component={SuppliersUtilityPicker}
        options={{
          headerStyle: {
            elevation: 0,
            backgroundColor: Colors.white
          }
        }}
      />
    </Stack.Navigator>
  )
}

const headerStyle = StyleSheet.create({
  title: {
    flex: 1,
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    color: '#000000',
    textAlign: 'center'
  }
})

export default UtilitiesNavigator
