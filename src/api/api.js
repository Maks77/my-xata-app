import axios from 'axios';
import {apiConfig} from '../config/config';

const api = axios.create({
    baseURL: apiConfig.baseUrl,
    headers: {
        'Content-Type': 'application/json'
    },
});

const debug = false

api.interceptors.request.use(function (config) {
    // Do something before request is sent
    debug && console.log('intercept_request', config)
    return config;
}, function (error) {
    debug && console.log('intercept_error', error)
    // Do something with request error
    return Promise.reject(error);
});


// Add a response interceptor
api.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    debug && console.log('intercept_response')
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});


export default api;
