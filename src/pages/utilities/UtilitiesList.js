import React, {useState, useEffect, useLayoutEffect} from 'react'
import {View, Text, Button, TouchableOpacity, FlatList, Image, StyleSheet} from 'react-native'
import utilityService from "../../api/utilityService";
import withStore from "../../utils/helpers/withStore";
import Color from "../../styles/colors";
import general from "../../styles/general";
import SearchHeaderButton from "../../components/atoms/SearchHeaderButton";

const UtilitiesList = (props) => {
  const {store, navigation, route} = props
  const {activeRealtyId} = route.params

  const [utilityCategories, setUtilityCategories] = useState([])

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitleStyle: {
        fontSize: 17,
        fontFamily: 'proxima-nova-regular',
        fontWeight: 'normal',
        color: '#000000',
        textAlign: 'center'
      },
      headerRight: () => (
        <SearchHeaderButton style={{marginRight: 16}} onPress={_onSearchPress} />
      ),
    });
  }, [navigation]);

  useEffect(() => {
    _getRealtyUtilCategories()
  }, [])

  const _getRealtyUtilCategories = () => {
    utilityService.getRealtyUtilityCategories(activeRealtyId).then(res => {
      console.log('_getRealtyUtilityCategories', res)
      if (res.status === 200 || res.status === 201) {
        setUtilityCategories(res.data)
      }
    })
  }

  const _onSearchPress = () => {
    console.log('Search pressed')
  }

  const _onPickUtilCategory = (category) => {
    console.log('_onPickUtilCategory', category)
    navigation.navigate('SuppliersList', {
      activeRealtyId,
      utilityCategoryId: category.id
    })
  }


  return (
    <View style={styles.container}>
      <View style={styles.roundedContainer}>
        <Text style={[general.roundSectionTitle, {marginBottom: 8}]}>Рекомендации по адресу</Text>

        <FlatList
          data={utilityCategories}
          keyExtractor={el => el.id}
          renderItem={({ item }) => <ListItem item={item} onPress={_onPickUtilCategory}/>}
        />
      </View>
    </View>
  )
}


const ListItem = ({item, onPress}) => {
  return (
    <TouchableOpacity onPress={() => onPress(item)}>
      <View style={listItemStyles.container}>
        <View style={listItemStyles.itemIconHolder}>
          <Image style={[listItemStyles.itemIcon, {backgroundColor: item.color || '#ccc'}]} source={{uri: item.icon}} />
        </View>
        <View style={listItemStyles.itemContentHolder}>
          <Text style={listItemStyles.itemTitle}>{item.name}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.accent
  },
  roundedContainer: {
    flex: 1,
    backgroundColor: Color.white,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingVertical: 19,
  }
})

const listItemStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 16
  },
  itemIconHolder: {
    width: 45,
    height: 45,
    borderRadius: 45/2,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ccc'
  },
  itemIcon: {
    resizeMode: 'contain',
    flex: 1,
    width: 45,
    height: 45
  },
  itemContentHolder: {
    justifyContent: 'space-around',
    marginLeft: 16
  },
  itemTitle: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#191919',
    fontWeight: 'normal'
  }
})

export default withStore(UtilitiesList)


// const utilityCategories = [
//   {
//     "id": "6521a71d-1f60-4551-8500-40af329817ea",
//     "unit_measure": {
//       "id": "08d1dd20-df2f-4692-9e2e-08c9592fead5",
//       "name": "кВт/ч"
//     },
//     "name": "Электроенергия",
//     "color": "#90F3D9",
//     "icon": "https://dev-4test.s3.amazonaws.com/my_xata/static/images/category_utility/6521a71d-1f60-4551-8500-40af329817ea.png"
//   },
//   {
//     "id": "8a223309-9285-4604-9ea7-0b97f0343b27",
//     "unit_measure": {
//       "id": "fc670aec-3669-4ddf-9d6d-e9a6071906ba",
//       "name": "м. куб."
//     },
//     "name": "Горячая вода",
//     "color": "#AAF390",
//     "icon": "https://dev-4test.s3.amazonaws.com/my_xata/static/images/category_utility/8a223309-9285-4604-9ea7-0b97f0343b27.png"
//   },
//   {
//     "id": "406ee631-1550-4031-8014-b82fa47ca865",
//     "unit_measure": {
//       "id": "e21ffbf1-c50e-41bc-97f6-2820aaba09f4",
//       "name": "кв. м."
//     },
//     "name": "Отопление",
//     "color": "#F390AA",
//     "icon": "https://dev-4test.s3.amazonaws.com/my_xata/static/images/category_utility/406ee631-1550-4031-8014-b82fa47ca865.png"
//   },
//   {
//     "id": "165315a7-9d60-4064-b024-8a9b606d1215",
//     "unit_measure": {
//       "id": "fc670aec-3669-4ddf-9d6d-e9a6071906ba",
//       "name": "м. куб."
//     },
//     "name": "Газ",
//     "color": "#F3A890",
//     "icon": "https://dev-4test.s3.amazonaws.com/my_xata/static/images/category_utility/165315a7-9d60-4064-b024-8a9b606d1215.png"
//   },
//   {
//     "id": "ebb5beff-5887-441e-953e-f6469941dbfb",
//     "unit_measure": {
//       "id": "fc670aec-3669-4ddf-9d6d-e9a6071906ba",
//       "name": "м. куб."
//     },
//     "name": "Холодная вода",
//     "color": "#000000",
//     "icon": "https://dev-4test.s3.amazonaws.com/my_xata/static/images/category_utility/ebb5beff-5887-441e-953e-f6469941dbfb.png"
//   }
// ]
