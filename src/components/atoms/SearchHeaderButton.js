import React, {useState, useEffect} from 'react'
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native'
import { EvilIcons } from '@expo/vector-icons';

const SearchHeaderButton = ({onPress, style}) => {
  return (
    <TouchableOpacity style={[style]} onPress={onPress}>
      <EvilIcons name="search" size={24} color="black" />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({})

export default SearchHeaderButton
