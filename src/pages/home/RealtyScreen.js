import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  Button,
  View,
  Text,
  TextInput,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native'
import Color from '../../styles/colors'
import PropertyCarousel from "../../components/property_carousel/PropertyCarousel";
import renderPagination from "../../components/carouselPagination";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import LocalStorage from "../../utils/LocalStorage";
import IconButton from "../../components/atoms/IconButton";
import RealtyForm from "./components/RealtyForm";
import withStore from "../../utils/helpers/withStore";
import realtyService from "../../api/realtyService";


const  RealtyScreen = (props) => {
  const {store, navigation} = props
  const [activeSlide, setSlide] = useState(0)
  const [showCarousel, setShowCarousel] = useState(true)
  const [editRealtyMode, setEditRealtyMode] = useState(false)

  useEffect(() => {
    _setKeyboardListeners()
    _fetchRealty()
    return () => {
      Keyboard.removeListener('keyboardDidShow', () => {})
      Keyboard.removeListener('keyboardDidHide', () => {})
    }
  }, [])

  useEffect(() => {
    store.realtyStore.updateActiveRealtyId(activeSlide)
  }, [activeSlide, store.realtyStore.realtyArr])

  const _onEditPress = () => {
    setEditRealtyMode(true)
  }

  const _setKeyboardListeners = () => {
    Keyboard.addListener('keyboardDidShow', (e) => {
      setShowCarousel(false)
    }, null)

    Keyboard.addListener('keyboardDidHide', (e) => {
      setShowCarousel(true)
    }, null)
  }

  const onAddRealtySuccess = (realtyFormData) => {
    Keyboard.dismiss()
    realtyService.addRealty(realtyFormData).then(res => {
      console.log('addRealty_success', res)
      if (res.status === 200 || res.status === 201) {
        store.realtyStore.addRealty(res.data)
        store.realtyStore.activeRealtyId = res.data.id
      }
    })
  }

  const _onEditRealty = (realty) => {
    realtyService.updateRealty(realty).then(res => {
      console.log('_onEditRealty', res)
      if (res.status === 200 || res.status === 201) {
        store.realtyStore.updateRealty(res.data)
        Keyboard.dismiss()
        setEditRealtyMode(false)
      }
    })
  }

  const _fetchRealty = () => {
    realtyService.getRealty().then(res => {
      if (res.status === 200) {
        store.realtyStore.setRealty(res.data)
      }
    })
  }

  const spoilToken = () => {
    store.authStore.access_token = '000';
    LocalStorage.setItem('access_token', '000').then(() => console.log('spoiled...'))
    LocalStorage.getItem('access_token').then((res) => {
      console.log('stored_token', res)
      console.log('mobx_token', store.authStore.access_token)
    })
  }

  const _removeRealtyPressed = (realty) => {
    store.realtyStore.removeRealty(realty.id)
    setEditRealtyMode(false)
  }

  return (
    <SafeAreaView style={styles.scrollContainer}>
      <StatusBar backgroundColor={Color.accent} barStyle={'dark-content'} translucent={false} hidden={false} />

      <View style={[styles.cardContainer, !showCarousel && styles.collapsedCardContainer]}>
        <PropertyCarousel
          realtyArr={store.realtyStore.realtyArr}
          onChangeActiveSlide={(i) => setSlide(i)}
          onEditPress={_onEditPress}
        />
        {renderPagination({slidesLength: store.realtyStore.realtyArr.length, activeSlide: activeSlide})}
      </View>

      <KeyboardAwareScrollView
        keyboardShouldPersistTaps='handled'
        style={[addPropStyle.container, {flexGrow: 3}]}
        behavior={'padding'}
        scrollEnabled={true}
        enableOnAndroid={true}
        enableAutomaticScroll={true}
        extraScrollHeight={16}
      >
        <Text style={{fontSize: 10}}>realty_id_active: {store.realtyStore.activeRealtyId}</Text>
        <Button title={'clear'} onPress={() => LocalStorage.clear().then(() => console.log('success'))}/>
        {/*<Button title={'spoil token'} onPress={() => spoilToken()}/>*/}

        <Button
          title={'list'}
          onPress={() => navigation.navigate('Utilities', {
            screen: 'UtilitiesList',
            params: {
              activeRealtyId: store.realtyStore.activeRealtyId
            }
          })}
        />


        {
          editRealtyMode || store.realtyStore.emptyRealty ?
            <RealtyForm
              realty={store.realtyStore.activeRealty}
              onCancelEditing={() => setEditRealtyMode(false)}
              addRealtySuccess={onAddRealtySuccess}
              onRemoveRealtyPress={_removeRealtyPressed}
              editRealty={_onEditRealty}
            />
            :
            <View>
              <Text style={addPropStyle.title}>Коммунальные</Text>
              <IconButton text={'Добавить услугу'}/>
            </View>
        }


      </KeyboardAwareScrollView>
    </SafeAreaView>
  )
}

const addPropStyle = StyleSheet.create({
  container: {
    flex: 3,
    paddingVertical: 20,
    paddingHorizontal: 16,
    backgroundColor: Color.white,
    overflow: 'scroll',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  title: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#828282',
    textAlign: 'center',
    marginBottom: 40
  }
})


const styles = StyleSheet.create({
  scrollContainer: {
    backgroundColor: Color.accent,
    flex: 1,
  },
  cardContainer: {
    flex: 2,
    justifyContent: 'flex-start'
  },
  collapsedCardContainer: {
    flex: 0,
    overflow: 'hidden',
    maxHeight: 0
  }
});

export default withStore(RealtyScreen);

