import React from 'react'
import {View, Text, Image, StyleSheet} from 'react-native'
import styled from 'styled-components'

const Logo = ({style}) => {

  return (
    <View style={[styles.contentHolder, style]}>
      <Image style={styles.logoImg} source={require('../../../assets/images/hata-logo.png')}/>
      <Text style={styles.logoText}>myXata</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  contentHolder: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoImg: {
    width: 89,
    height: 97,
    resizeMode: 'contain'
  },
  logoText: {
    fontFamily: 'comfortaa',
    fontWeight: 'normal',
    fontSize: 43,
    lineHeight: 43,
    color: '#333',
    marginTop: 29
  }
})

export default Logo
