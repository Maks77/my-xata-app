const debug = false

const authInterceptor = (access_token, callback) => {
  return (config) => {
    if (!config.url.includes('/oauth2/token/')) {
      config.headers['Authorization'] = 'Bearer ' + access_token;
      debug && console.log('intercepted_request', config)
    }

    callback ? callback(config) : () => {}
    return config
  }
}

export default authInterceptor
