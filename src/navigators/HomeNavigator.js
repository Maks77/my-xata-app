import React from 'react'
import {Text, View, Button, TouchableOpacity, StyleSheet} from 'react-native'
import {createStackNavigator} from '@react-navigation/stack';
import Colors from '../styles/colors'
import {MaterialCommunityIcons} from '@expo/vector-icons';
import Icon from '../utils/icon'
// Screens
import RealtyScreen from "../pages/home/RealtyScreen";
import UtilitiesList from "../pages/utilities/UtilitiesList";
import SuppliersList from "../pages/utilities/SuppliersList";
import SuppliersUtilityPicker from "../pages/utilities/SuppliersUtilityPicker";

const Stack = createStackNavigator();

const HomeNavigator = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerMode: 'screen',
        headerHideShadow: true,
        headerStyle: {
          elevation: 0,
          backgroundColor: Colors.accent,
        },
        headerTitleStyle: {
          fontSize: 17,
          fontFamily: 'proxima-nova-regular',
          fontWeight: 'normal',
          color: '#000000'
        },
      }}
    >

      <Stack.Screen
        options={{
          headerTitle: props => <Header/>
        }}
        name="AddPropertyScreen" component={RealtyScreen}
      />
      <Stack.Screen
        title={'Выбор услуг'}
        name={'UtilitiesList'}
        component={UtilitiesList}
      />
      <Stack.Screen
        title={'Поиск по компании'}
        name={'SuppliersList'}
        component={SuppliersList}
        options={{
          headerStyle: {
            elevation: 0,
            backgroundColor: '#fff'
          }
        }}
      />
      <Stack.Screen
        title={'company name (set)'}
        name={'SuppliersUtilityPicker'}
        component={SuppliersUtilityPicker}
        options={{
          headerStyle: {
            elevation: 0,
            backgroundColor: Colors.white
          }
        }}
      />
    </Stack.Navigator>
  )
}


// todo: refactor later
const Header = () => {
  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <HeaderLeft/>
      <Text style={headerStyle.title}>Недвижимость</Text>
      <HeaderRight/>
    </View>
  )
}

const HeaderRight = () => {
  return (
    <TouchableOpacity>
      <MaterialCommunityIcons name="bell-outline" size={24} color="black"/>
    </TouchableOpacity>
  )
}

const HeaderLeft = () => {
  return (
    <TouchableOpacity>
      <Icon name="user-icon" size={24} color="black"/>
    </TouchableOpacity>
  )
}

const headerStyle = StyleSheet.create({
  title: {
    flex: 1,
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    color: '#000000',
    textAlign: 'center'
  }
})

export default HomeNavigator
