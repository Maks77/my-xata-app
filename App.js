import 'react-native-gesture-handler';
import 'mobx-react-lite/batchingForReactNative'
import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';

import {Provider} from 'mobx-react'
import {Store} from "./src/store/store";

import MainNavigator from "./src/navigators/MainNavigator";
import LoginNavigator from "./src/navigators/LoginNavigator";
import LocalStorage from "./src/utils/LocalStorage";

import api from './src/api/api'
import authInterceptor from "./src/api/interceptors/auth.interceptor";

const fetchFonts = () => {
  return Font.loadAsync({
    'roboto-bold': require('./assets/fonts/Roboto-Bold.ttf'),
    'roboto-light': require('./assets/fonts/Roboto-Light.ttf'),
    'roboto-thin': require('./assets/fonts/Roboto-Thin.ttf'),
    'proxima-nova-black': require('./assets/fonts/ProximaNova-Black.otf'),
    'proxima-nova-bold': require('./assets/fonts/ProximaNova-Bold.otf'),
    'proxima-nova-extrabold': require('./assets/fonts/ProximaNova-Extrabold.otf'),
    'proxima-nova-thin': require('./assets/fonts/ProximaNova-Thin.otf'),
    'proxima-nova-regular': require('./assets/fonts/ProximaNova-Regular.otf'),
    'comfortaa': require('./assets/fonts/Comfortaa.ttf'),
    'icomoon': require('./assets/fonts/icomoon.ttf')
  });
}

const store = new Store()

export default function App(props) {
  const [dataLoaded, setDataLoaded] = useState(false);
  const [visited, setVisited] = useState(false)

  const loadData = () => {
    return fetchFonts().then(() => {
      return restoreData()
    })
  }

  const restoreData = () => {
    return Promise.all([
      LocalStorage.getItem('phoneNumber'),
      LocalStorage.getItem('access_token'),
      LocalStorage.getItem('refresh_token')
    ]).then(res => {
      console.log('restoreData', res)
      if (res && res.every(el => el !== null)) {
        console.log('setDataToMobXStore')
        const [phoneNumber, access_token, refresh_token] = res
        store.authStore.phoneNumber = phoneNumber
        store.authStore.access_token = access_token
        store.authStore.refresh_token = refresh_token
        setVisited(true)
      } else {
        console.log('store_is_empty')
      }
    })
  }

  if (!dataLoaded) {
    return <AppLoading
      startAsync={loadData}
      onFinish={() => setDataLoaded(true)}
      onError={(err) => console.log(err)}
    />
  }
  return (
    <Provider store={store}>
      <MainNavigator visited={visited} />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'proxima-nova-black'
  }
});

