import React from 'react'
import {View, Text, TextInput, StyleSheet, StatusBar, SafeAreaView} from 'react-native'
import {LinkButton, Logo} from "../../components/atoms";
import Color from "../../styles/colors";


const SplashScreen = (props) => {
  const {navigation} = props
  const onNextPressed = () => {
    navigation.navigate('IntroScreen')
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Color.transparent} barStyle={'dark-content'} hidden={false} />
      <Logo />
      <LinkButton style={{marginTop: 40}} text={'Next'} onPress={onNextPressed} />
    </SafeAreaView>
  )
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  }
})

export default SplashScreen
