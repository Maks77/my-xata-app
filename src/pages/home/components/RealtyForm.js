import React, {useRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Alert,
  TouchableOpacity
} from 'react-native'
import UnderlinedInput from "../../../components/UnderlinedInput";
import YellowButton from "../../../components/atoms/YellowButton";
import {LinkButton} from "../../../components/atoms";
import realtyService from "../../../api/realtyService";
import { Feather } from '@expo/vector-icons';

const RealtyForm = ({realty, onCancelEditing, addRealtySuccess, onRemoveRealtyPress, editRealty}) => {
  const [formData, setFormData] = useState({
    id: null,
    realty_type: '',
    city: '',
    street: '',
    house_number: '',
    building_number: '',
    apartment_number: ''
  })

  useEffect(() => {
    if (realty && realty.id !== 'custom_empty') {
      setFormData(realty)
    } else {
      setFormData({
        id: null,
        realty_type: '',
        city: '',
        street: '',
        house_number: '',
        building_number: '',
        apartment_number: ''
      })
    }
  }, [realty])

  const _handleInput = (key, value) => {
    setFormData(prevState => ({
      ...prevState,
      [key]: value
    }))
  }

  const _onSubmitPressed = () => {
    if (_isValid()) {
      submit()
    } else {
      Alert.alert('Alert', 'invalid')
    }
  }

  const submit = () => {
    // Alert.alert('Alert', 'success')

    if (realty.id === 'custom_empty') {
      addRealtySuccess(formData)
    } else {
      editRealty(formData)
    }
  }

  const _isValid = () => {
    return (
      formData.city.trim() !== ''
      && formData.street.trim() !== ''
      && formData.house_number.trim() !== ''
    )
  }

  return (
    <View style={formStyle.container}>

      <View style={formStyle.titleHolder}>
        <Text style={[formStyle.title, {marginBottom: 30}]}>Добавление недвижимости</Text>
        {realty && realty.id !== 'custom_empty' && (
          <View style={formStyle.deleteIcon}>
            <TouchableOpacity onPress={() => onRemoveRealtyPress(realty)}>
              <Feather name="trash" size={18} color="#EB5757" />
            </TouchableOpacity>
          </View>
        )}
      </View>


      <UnderlinedInput
        returnKeyType={'next'}
        style={formStyle.lineInput}
        placeholder={'Город*'}
        value={formData.city}
        onChangeText={(t) => _handleInput('city', t)}
      />
      <UnderlinedInput
        returnKeyType={'next'}
        style={formStyle.lineInput}
        placeholder={'Улица*'}
        value={formData.street}
        onChangeText={(t) => _handleInput('street', t)}
      />
      <View style={formStyle.row}>
        <UnderlinedInput
          returnKeyType={'next'}
          style={formStyle.rowInput}
          placeholder={'Дом*'}
          value={formData.house_number}
          onChangeText={(t) => _handleInput('house_number', t)}
        />
        <UnderlinedInput
          returnKeyType={'next'}
          style={formStyle.rowInput}
          placeholder={'Корпус'}
          value={formData.building_number}
          onChangeText={(t) => _handleInput('building_number', t)}
        />
        <UnderlinedInput
          returnKeyType={'done'}
          style={formStyle.rowInput}
          placeholder={'Кв'}
          value={formData.apartment_number}
          onChangeText={(t) => _handleInput('apartment_number', t)}
        />
      </View>

      <YellowButton onPress={_onSubmitPressed} style={{marginTop: 60, marginBottom: 8}} disabled={false} text={'Cохранить'} />
      {realty.id !== 'custom_empty'
        && <LinkButton onPress={onCancelEditing} style={{alignItems: 'center'}} text={'Отмена'} />
      }

    </View>
  )
}

const formStyle = StyleSheet.create({
  container: {
    paddingBottom: 50
  },
  titleHolder: {},
  deleteIcon: {
    position: 'absolute',
    top: 0,
    right: 0
  },
  title: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 15,
    color: '#828282',
    textAlign: 'center'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: -16
  },
  lineInput: {
    marginBottom: 10
  },
  rowInput: {
    width: 'auto',
    flex: 1,
    marginHorizontal: 16
  }
})

export default RealtyForm;
