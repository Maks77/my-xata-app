import {AsyncStorage} from 'react-native'

const setItem = async (key, value) => {
  return await AsyncStorage.setItem(key, value)
}

const getItem = async (key) => {
  return await AsyncStorage.getItem(key)
}

const clear = async () => {
  return await AsyncStorage.clear()
}

export default {
  setItem,
  getItem,
  clear
}
