import axios from "axios";
import {replaceNavigation} from "../../navigators/RootNavigation";
import LocalStorage from "../../utils/LocalStorage";
import {apiConfig} from "../../config/config";
import api from "../api";

const debug = false

const refreshTokenInterceptor = (store, cb) => {
    debug && console.log('refreshTokenInterceptor run...', store)
    return (error) => {
        debug && console.log('refreshTokenInterceptor_ERROR', error)
        const originalRequest = error.config;
        if (!error.response) {
            cb ? cb() : () => {}
            return Promise.reject('Network Error')
        } else if ((error.response.status === 401) && !originalRequest._retry) {
            debug && console.log(`====== RefreshLogic here ======`)
            originalRequest._retry = true;
            const {phoneNumber, refresh_token} = store.authStore
            return refreshToken(phoneNumber, refresh_token)
                .then((refreshRes) => {
                    debug && console.log('refreshRes', refreshRes)
                    const {access_token, refresh_token} = refreshRes.data
                    store.authStore.setAuthData(refreshRes.data)
                    return saveRefreshToken(access_token, refresh_token)
                        .then(() => {
                            return refreshRes
                        })
                })
                .then((refreshRes) => {
                    debug && console.log('[4] refreshRes', refreshRes)
                    originalRequest.headers['Authorization'] = 'Bearer ' + refreshRes.data.access_token;
                    debug && console.log('[5] originalRequest', originalRequest)
                    return axios(originalRequest)
                      .catch(e => {
                          cb ? cb() : () => {}
                          debug && console.log('error_logout', e)
                          replaceNavigation('LogoutNavigator')
                      })
                })
        } else {
            cb ? cb() : () => {}
            return error.response
        }

    }
}

const refreshToken = (phoneNumber, refresh_token) => {
    debug && console.log('[3] refreshToken run...')
    const formData = new FormData();
    formData.append('username', phoneNumber);
    formData.append('grant_type', 'refresh_token');
    formData.append('client_id', apiConfig.client_id);
    formData.append('client_secret', apiConfig.client_secret);
    formData.append('refresh_token', refresh_token);

    return api({
        method: 'post',
        url: '/oauth2/token/',
        data: formData,
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    })
}

const saveRefreshToken = (access_token, refresh_token) => {
    debug && console.log('saveRefreshToken')
    return Promise.all([
        LocalStorage.setItem('access_token', access_token),
        LocalStorage.setItem('refresh_token', refresh_token)
    ]).catch(e => console.log('saveRefreshToken_error', e))
}

export default refreshTokenInterceptor
