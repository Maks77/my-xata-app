import api from "../api/api";
import {AuthStore} from "./AuthStore";
import {action, computed, observable} from "mobx";
import realtyService from "../api/realtyService";

const realty_empty = {
  id: "custom_empty",
  realty_type: "string",
  city: "string",
  street: "string",
  house_number: "string",
  building_number: "string",
  apartment_number: "string",
  snapshot: "string"
}

export class RealtyStore {

  constructor() {
    this.authStore = new AuthStore()
  }

  @observable activeRealtyId = null;
  @observable editActiveRealty = false
  @observable realtyArr = []


  @computed get activeRealty() {
    return this.realtyArr.find(el => el.id === this.activeRealtyId)
  }

  @computed get emptyRealty() {
    return this.activeRealtyId === 'custom_empty'
  }

  @computed get activeSlideIndex() {
    const ind = this.realtyArr.map(el => el.id).indexOf(this.activeRealtyId)
    return ind > 0 ? ind : 0
  }


  // Realty card actions (carousel logic)
  @action updateActiveRealtyId = (cardIndex) => {
    this.activeRealtyId = this.realtyArr.length > 0 ? this.realtyArr[cardIndex].id : null
  }

  @action setEditActiveRealty = (value) => {
    this.editActiveRealty = value
  }



  // Realty -> basic methods
  @action addRealty = (newRealty) => {
    this.realtyArr = [
      ...this.realtyArr.filter(el => el.id !== 'custom_empty'),
      newRealty,
      realty_empty
    ]
  }

  @action updateRealty = (realty) => {
    const index = this.realtyArr.map(el => el.id).indexOf(realty.id)
    this.realtyArr[index] = realty
  }

  @action removeRealty = (realtyId) => {
    return realtyService.deleteRealty(realtyId)
      .then(res => {
        if (res.status === 200) {
          this.realtyArr = this.realtyArr.filter(el => el.id !== realtyId)
        }
        return res
      })
  }

  // Set array of realty on first load
  @action setRealty = (realties) => {
    if (realties.length > 0) {
      this.realtyArr = [ ...realties, realty_empty ]
    } else {
      this.realtyArr = [ realty_empty ]
    }
  }

  // test method
  @action getRealtyTypes = () => {
    return api.get('/api/realty-type/')
  }
}
