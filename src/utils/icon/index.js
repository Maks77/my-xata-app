import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../../../assets/fonts/icomoon_config.json';

// We use the IcoMoon app (https://icomoon.io) to generate a custom font made up
// of SVG icons. The actual font file is loaded up-front in src/index.js.
export default createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
