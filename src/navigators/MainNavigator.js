import React from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {navigationRef} from './RootNavigation';
import withStore from "../utils/helpers/withStore";
import {
    useRefreshInterceptor,
    useRefreshLayoutInterceptor,
    useTokenInterceptor,
    useTokenLayoutInterceptor
} from "../utils/helpers/useInterceptors";


// Navigators
import IntroNavigator from "./IntroNavigator";
import LoginNavigator from "./LoginNavigator";
import AccountNavigator from "./AccountNavigator";
import HomeNavigator from "./HomeNavigator";
import LogoutNavigator from "./LogoutNavigator";
import UtilitiesNavigator from "./UtilitiesNavigator";



// enableScreens();
const Stack = createStackNavigator();

const MainNavigator = (props) => {
  const {visited, store} = props;
  console.log('MainNavigator_visited', visited);

  useTokenLayoutInterceptor(store.authStore.access_token)
  useTokenInterceptor(store.authStore.access_token)
  useRefreshLayoutInterceptor(store)
  useRefreshInterceptor(store, store.authStore.access_token)

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={{headerShown: false}}>

        {/*navigators*/}
        {!visited && <Stack.Screen name="IntroNavigator" component={IntroNavigator}/>}
        {!visited && <Stack.Screen name="LoginNavigator" component={LoginNavigator}/>}

        <Stack.Screen name="HomeNavigator" component={HomeNavigator}/>


        <Stack.Screen name="Utilities" component={UtilitiesNavigator}/>


        <Stack.Screen name="AccountNavigator" component={AccountNavigator}/>

        {/*If refreshToken failed logout and navigate to LoginNavigator*/}
        <Stack.Screen name="LogoutNavigator" component={LogoutNavigator}/>

      </Stack.Navigator>
    </NavigationContainer>
  )
}

// export default MainNavigator
export default withStore(MainNavigator)
