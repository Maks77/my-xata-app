import React, {useState, useEffect} from 'react'
import {View, Text, FlatList, ScrollView, TouchableOpacity, KeyboardAvoidingView, StyleSheet} from 'react-native'
import utilityService from "../../api/utilityService";
import UnderlinedInput from "../../components/UnderlinedInput";
import {EvilIcons} from "@expo/vector-icons";
import Color from "../../styles/colors";

const SuppliersList = (props) => {
  const {route, navigation} = props
  const {activeRealtyId, utilityCategoryId} = route.params
  const [suppliers, setSuppliers] = useState([])
  const [cacheSuppliers, setCacheSuppliers] = useState([])

  useEffect(() => {
    _getSuppliersList()
  }, [])

  const _getSuppliersList = () => {
    utilityService.getRealtySuppliersByCategory(activeRealtyId, utilityCategoryId)
      .then(res => {
        console.log('_getSuppliersList', res)
        if (res.status === 200) {
          setSuppliers(res.data)
          setCacheSuppliers(res.data)
        }
      })
  }

  const _searchBarChange = (text) => {
    const searchedSuppliers = cacheSuppliers.filter(el => el.name.toLowerCase().indexOf(text.toLowerCase()) !== -1)
    setSuppliers(searchedSuppliers)
  }

  const _onPickSupplier = (supplier) => {
    console.log('pickSupplier', supplier)
    navigation.navigate('SuppliersUtilityPicker', {
      supplier
    })
  }

  return (
    <View keyboardShouldPersistTaps='handled' style={styles.container}>
      <View style={styles.searchBar}>
        <UnderlinedInput
          rightIcon={() => (
            <EvilIcons name="search" size={24} color="black" />
          )}
          placeholder={'Поиск по компании'}
          onChangeText={_searchBarChange}
        />
      </View>

      <FlatList
        data={suppliers}
        renderItem={({ item }) => <SupplierItem onPress={_onPickSupplier} supplier={item} />}
        keyExtractor={item => item.id}
      />
    </View>
  )
}

const SupplierItem = ({supplier, onPress}) => {
  return (
    <TouchableOpacity onPress={() => onPress && onPress(supplier)}>
      <View style={{padding: 16}}>
        <Text style={{fontFamily: 'proxima-nova-regular', fontSize: 17, color: '#828282'}}>{supplier.name}</Text>
      </View>
    </TouchableOpacity>
  )
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.white
  },
  searchBar: {
    paddingHorizontal: 16
  }
})

export default SuppliersList
