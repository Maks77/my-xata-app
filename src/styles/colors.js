export default {
  accent: '#FFAD08',

  white: '#ffffff',
  black: '#000000',
  transparent: 'transparent'
}
