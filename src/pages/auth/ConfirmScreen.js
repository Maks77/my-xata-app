import React, {useState, useEffect} from 'react'
import {View, Text, Image, StyleSheet, TextInput, StatusBar, SafeAreaView, AsyncStorage, Alert} from 'react-native'
import SmoothPinCodeInput from "react-native-smooth-pincode-input";
import Color from '../../styles/colors'
import {LinkButton} from "../../components/atoms";
import general from "../../styles/general";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import LocalStorage from '../../utils/LocalStorage'
import loginService from "../../api/loginService";
import withStore from "../../utils/helpers/withStore";


const ConfirmScreen = (props) => {
  const {store, navigation} = props

  const [confirmCode, setConfirmCode] = useState('')
  const [mPhoneNumber, setPhoneNumber] = useState('')
  const [timeLeft, setTimeLeft] = useState(15)
  const [timerActive, setTimerActive] = useState(false)

  useEffect(() => {
    let interval = null;
    if (timerActive && timeLeft > 0) {
      interval = setInterval(() => {
        setTimeLeft(timeLeft - 1);
      }, 1000);
    } else {
      setTimerActive(false)
      clearInterval(interval)
    }
    return () => clearInterval(interval);
  }, [timeLeft, timerActive])

  useEffect(() => {
    console.log('onCreate_confirmScreen_phone', store.authStore.phoneNumber)
    _getConfirmCode(store.authStore.phoneNumber)
  }, [])

  const _onFulFill = (text) => {
    store.authStore.confirmCode = text
    _getToken()
  }


  const _onNextPress = () => {
    _getToken();
  }

  const _getToken = () => {
    store.authStore.token().then(res => {
      console.log('gotToken', res)
      if (res.status === 200) {
        _persistAuthData(res.data).then(() => {
          console.log('got&store access token', res.data.access_token)
          console.log('got&store refresh token', res.data.refresh_token)
          navigation.navigate('PasswordScreen')
        })
      } else {
        Alert.alert('Error', 'Wrong sms code')
      }
    })
  }


  // todo: move to LocalStorage
  const _persistAuthData = (data) => {
    return Promise.all([
      LocalStorage.setItem('access_token', data.access_token),
      LocalStorage.setItem('refresh_token', data.refresh_token)
    ])
  }

  const _resendSms = () => {
    setConfirmCode('')
    setTimeLeft(15)
    _getConfirmCode(store.authStore.phoneNumber)
    startCountDownTimer()
  }

  const startCountDownTimer = () => {
    setTimerActive(true)
  }


  const _getConfirmCode = (phoneNumber) => {
    store.authStore.auth(phoneNumber)
      .then(res => {
        if (res.status === 200) {
          const confirmCode = res.data.otp_code ? res.data.otp_code : ''
          if (confirmCode !== '') {
            setConfirmCode(confirmCode)
          }
        }
      })
  }


  return (
    <KeyboardAwareScrollView
      style={{backgroundColor: Color.white}}
      contentContainerStyle={{flexGrow: 1}}
      behavior={'position'}
      scrollEnabled={true}
      enableOnAndroid={true}
      enableAutomaticScroll={true}
      extraScrollHeight={40}
    >
      <StatusBar backgroundColor={Color.white} barStyle={'dark-content'} translucent={false} hidden={false}/>
      <View style={styles.halfContainer}>
        <Image style={styles.bgImage} source={require('../../../assets/images/confirm_bg.png')}/>
      </View>
      <View style={[styles.halfContainer, {justifyContent: 'flex-start'}]}>
        <Text style={styles.confirmText}>Введите код который прийдет в смс</Text>

        <View style={styles.confirmHolder}>
          <SmoothPinCodeInput
            value={confirmCode}
            onTextChange={code => setConfirmCode(code)}
            onFulfill={_onFulFill}
            textStyle={{
              fontFamily: 'comfortaa',
              fontSize: 24,
              color: '#333333',
            }}
            cellStyle={{
              borderBottomWidth: 2,
              borderColor: 'gray',
              flex: 1,
              width: 30
            }}
            cellSpacing={10}
            cellStyleFocused={{
              borderColor: Color.accent
            }}
            codeLength={6}
          />

          {
            timerActive ? (
              <View style={{marginTop: 20}}>
                <Text style={[general.smallTextGray]}>Отправить смс еще раз через</Text>
                <Text style={[general.smallTextGray]}>{timeLeft} сек</Text>
              </View>
            ) : (
              <LinkButton onPress={_resendSms} style={{marginTop: 20}} text={'Отправить смс еще раз'}
                          textColor={Color.accent}/>
            )
          }

          <LinkButton onPress={_onNextPress} style={{marginTop: 10}} text={'next'} textColor={Color.accent}/>
        </View>

      </View>
    </KeyboardAwareScrollView>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  halfContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bgImage: {},
  confirmText: {
    fontFamily: 'proxima-nova-regular',
    fontSize: 17,
    textAlign: 'center',
    color: '#333',
    fontWeight: 'normal',
    paddingHorizontal: 40
  },


  confirmHolder: {
    alignItems: 'center',
  }
})


// export default ConfirmScreen
export default withStore(ConfirmScreen)
