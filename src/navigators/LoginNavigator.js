import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';


// Screens
import LoginScreen from "../pages/auth/LoginScreen";
import ConfirmScreen from "../pages/auth/ConfirmScreen";
import PasswordScreen from "../pages/auth/PasswordScreen";

const Stack = createStackNavigator();

const LoginNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{
      headerHideShadow: true,
      headerStyle: {
        elevation: 0
      },
      headerTitleStyle: {
        fontSize: 17,
        fontFamily: 'proxima-nova-regular',
        fontWeight: 'normal',
        color: '#000000'
      },
    }}>
        <Stack.Screen name="LoginScreen" component={LoginScreen} options={{headerShown: false}}/>
        <Stack.Screen name="ConfirmScreen" component={ConfirmScreen}/>
        <Stack.Screen name="PasswordScreen" component={PasswordScreen}/>
    </Stack.Navigator>
  )
}

export default LoginNavigator
