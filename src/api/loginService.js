import api from "./api";
import {apiConfig} from "../config/config";


const auth = (phoneNumber) => {
    const body = {
        user_phone: phoneNumber,
    }
    return api.post('/oauth2/get-otp/', body)
}


const token = (phoneNumber, password) => {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    const formData = new FormData();
    formData.append('username', phoneNumber);
    formData.append('password', password);
    formData.append('grant_type', 'password');
    formData.append('client_id', apiConfig.client_id);
    formData.append('client_secret', apiConfig.client_secret);

    return api.post('/oauth2/token/', formData, {headers: headers})
}

// const refreshToken = () => {
//     const headers = {'Content-Type': 'application/x-www-form-urlencoded'}
//     const urlencoded = new URLSearchParams();
//     urlencoded.append('username', this.authQuery.getValue().phone_number);
//     urlencoded.append('grant_type', 'refresh_token');
//     urlencoded.append('client_id', environment.client_id);
//     urlencoded.append('client_secret', environment.client_secret);
//     urlencoded.append('refresh_token', refreshToken ? refreshToken : '');
// }

const getUser = () => {
    return api.get('/api/user')
}

export default {
    auth,
    token,
    getUser
}
