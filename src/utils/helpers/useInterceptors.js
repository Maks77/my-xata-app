import {useState, useEffect, useLayoutEffect} from 'react'
import api from "../../api/api";
import authInterceptor from "../../api/interceptors/auth.interceptor";
import refreshTokenInterceptor from "../../api/interceptors/refreshToken.interceptor";

const createRequestInterceptor = (access_token, cb) => {
  return api.interceptors.request.use(
    authInterceptor(access_token, cb),
    (error) => Promise.reject(error)
  )
}

const createResponseInterceptor = (store, cb) => {
  return api.interceptors.response.use(
    (response) => {
      return response
    },
    refreshTokenInterceptor(store, cb)
  )
}



export const useTokenLayoutInterceptor = (access_token) => {
  useLayoutEffect(() => {
    const reqIntc = createRequestInterceptor(access_token, (cb) => {
      api.interceptors.request.eject(reqIntc)
    })
  }, [])
}


export const useTokenInterceptor = (access_token) => {
  const [requestInterceptor, setRequestInterceptor] = useState(null)
  useEffect(() => {
    if (requestInterceptor) {
      api.interceptors.request.eject(requestInterceptor)
      setRequestInterceptor(null)
    }
    const reqIntc = createRequestInterceptor(access_token)
    setRequestInterceptor(reqIntc)
  }, [access_token])
}

export const useRefreshLayoutInterceptor = (store) => {
  useLayoutEffect(() => {
    const responseInterceptor = createResponseInterceptor(store, () => {
      api.interceptors.response.eject(responseInterceptor)
    })
  }, [])
}

export const useRefreshInterceptor = (store, watchValue) => {
  const [responseInterceptor, setResponseInterceptor] = useState(null)
  useEffect(() => {
    if (responseInterceptor) {
      api.interceptors.response.eject(responseInterceptor)
      setResponseInterceptor(null)
    }
    const respInterceptor = createResponseInterceptor(store)
    setResponseInterceptor(respInterceptor)
  }, [watchValue])
}



// export const useInterceptors = (store, watchValue) => {
//   console.log('!!!>>useInterceptors<<!!!', watchValue)
//   const [requestInterceptor, setRequestInterceptor] = useState(null)
//   const [responseInterceptor, setResponseInterceptor] = useState(null)
//
//   const setupInterceptors = (access_token) => {
//     if (requestInterceptor) {
//       api.interceptors.request.eject(requestInterceptor)
//       setRequestInterceptor(null)
//       console.log('eject_req_interceptor', requestInterceptor)
//     }
//     const reqIntc = createRequestInterceptor(access_token)
//     setRequestInterceptor(reqIntc)
//     console.log('reqIntc', reqIntc)
//
//     if (responseInterceptor) {
//       api.interceptors.response.eject(responseInterceptor)
//       setResponseInterceptor(null)
//       console.log('eject_req_interceptor', responseInterceptor)
//     }
//     const respInterceptor = createResponseInterceptor(store)
//     setResponseInterceptor(respInterceptor)
//
//     console.log('interceptors', api.interceptors)
//     return () => {
//       if (requestInterceptor) {
//         api.interceptors.request.eject(requestInterceptor)
//         setRequestInterceptor(null)
//       }
//       if (responseInterceptor) {
//         api.interceptors.response.eject(responseInterceptor)
//         setResponseInterceptor(null)
//       }
//     }
//   }
//
//   useLayoutEffect(() => {
//     console.log('------------USE_LAYOUT_EFFECT------------')
//     setupInterceptors(store.authStore.access_token)
//   }, [])
//
//   useEffect(() => {
//     console.log('------------USE_EFFECT------------')
//     setupInterceptors(store.authStore.access_token)
//   }, [watchValue])
// }
